<?php
/* Enable support for custom logo. */

	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );


register_nav_menus( array(
	'primary_top' => __( 'Primary Menu Top', 'maleo' ),
	'primary_bottom' => __( 'Primary Menu Bottom', 'maleo' ),
    'search_menu' => __( 'Search menu', 'maleo' ),
) );



function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Register thumbnails
	add_theme_support( 'post-thumbnails' );
    add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode
	add_image_size( 'kamienie-thumb', 400, 400 ); // Soft Crop Mode for kamienie img

//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_page('Stopka');
	acf_add_options_page('Kamienie - przycisk');
	acf_add_options_page('Atrybuty - listy rozwijane');
	acf_add_options_page('Dodatkowe pola w produkcie - Etykiety');
}



function wpb_widgets_init() {
 
    register_sidebar( array(
        'name' => __( 'Widget filrtowanie', 'maleo_rrs' ),
        'id' => 'sidebar-filter',
        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    }
 
add_action( 'widgets_init', 'wpb_widgets_init' );





// Dodawanie nowego typu postów Usługi (Kamienie) START 
// function create_kamienie_posttype(){

//     $labels = array(
// 			'name' => __('kamienie'),
// 			'singular_name' => __('kamienie'),
// 			'add_new' => __('Dodaj kamień'),
// 			'add_new_item' => __('Dodaj nowy kamień'),
// 			'edit_item' => __('Edytuj kamień'),
// 			'new_item' => __('Nowy kamień'),
// 			'view_item' => __('Zobacz kamień'),
// 			'search_items' => __('Szukaj kamieni'),
// 			'not_found' => __('Nie Znaleziono kamieni'),
// 			'not_found_in_trash' => __('Brak kamieni w koszu'),
// 			'all_items' => __('Wszystkie kamienie'),
// 			'archives' => __('Zarchiwizowane kamienie'),
// 			'insert_into_item' => __('Dodaj do kamienie'),
// 			'uploaded_to_this_item' => __('Sciągnięto do bieżącego kamienia'),
// 			'featured_image' => __('Zdjęcie kamienia'),
// 			'set_featured_image' => __('Ustaw Zdjęcie kamienia'),
// 			'remove_featured_image' => __('Usuń Zdjęcie kamienia'),
// 			'use_featured_image' => __('Użyj zdjęcia kamienia'),
// 			'menu_name' => __('kamienie')
//     );


// 	$args = array(
// 		'label' => 'kamienie',
// 		'labels' => $labels,
// 		'description' => __('Typ Postu zawiera treść dla kamienia.'),
// 		'public' => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable' => true,
// 		'show_ui' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_menu' => true,
// 		'show_in_rest' => true,
// 		'show_in_admin_bar' => true,
// 		'menu_position' => 18,
// 		'menu_icon' => 'dashicons-screenoptions',
// 		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes', 'custom-fields'),
// 		'has_archive' => false,
// 		'hierarchical' => true,
// 		'rewrite' => array('slug'=>'kamienie','with_front'=>true),
// 		'capabilities' => array(
// 			'edit_post'          => 'update_core',
// 			'read_post'          => 'update_core',
// 			'delete_post'        => 'update_core',
// 			'edit_posts'         => 'update_core',
// 			'edit_others_posts'  => 'update_core',
// 			'delete_posts'       => 'update_core',
// 			'publish_posts'      => 'update_core',
// 			'read_private_posts' => 'update_core'
// 		),
// 	);
// 	register_post_type('kamienie', $args);
// }

// add_action('init','create_kamienie_posttype', 0);

// function register_taxonomy_kamienie_category() {
//     $labels = [
//         'name'              => _x('Categories', 'taxonomy general name'),
//         'singular_name'     => _x('Category', 'taxonomy singular name'),
//         'search_items'      => __('Search Categories'),
//         'all_items'         => __('All Categories'),
//         'parent_item'       => __('Parent Category'),
//         'parent_item_colon' => __('Parent Category:'),
//         'edit_item'         => __('Edit Category'),
//         'update_item'       => __('Update Category'),
//         'add_new_item'      => __('Add New Category'),
//         'new_item_name'     => __('New Category Name'),
//         'menu_name'         => __('Kategorie'),
//         ];

//         $args = [
//         'hierarchical'      => true, // make it hierarchical (like categories)
//         'labels'            => $labels,
// 		'show_ui'           => true,
// 		'label'             => 'My Taxonomy',
//         'show_admin_column' => true,
// 		'query_var'         => true,
// 		'show_ui' => true,
// 		'show_in_rest' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_admin_bar' => true,
//         'rewrite'           => ['slug' => 'kategoria'],
//         ];

//         register_taxonomy('kategoria', ['kamienie'], $args);
// }
// add_action('init', 'register_taxonomy_kamienie_category');
// Dodawanie nowego typu postów END












// Registr CPT Kamienie



function kamienie_post_type() {
    $labels = array(
        'name'                => 'kamienie',
        'singular_name'       => 'kamienie',
        'menu_name'           => 'kamienie',
        'parent_item_colon'   => 'Nadrzędne kamienie',
        'all_items'           => 'Wszystkie kamienie',
        'view_item'           => 'Zobacz kamienie',
        'add_new_item'        => 'Dodaj kamienie',
        'add_new'             => 'Dodaj nowy kamień',
        'edit_item'           => 'Edytuj kamień',
        'update_item'         => 'Aktualizuj',
        'search_items'        => 'Szukaj kamienia',
        'not_found'           => 'Nie znaleziono',
        'not_found_in_trash'  => 'Nie znaleziono'
    ); 
    $args = array(
        'label' => 'kamienie',
        'rewrite' => array(
            'slug' => 'kamienie'
        ),
        'description'         => 'kamienie',
        'labels'              => $labels,
        'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true, 
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon'           => 'dashicons-id-alt',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'kamienie', $args );
} 
add_action( 'init', 'kamienie_post_type', 0 );







function register_taxonomy_category_kamienie() {
    $labels = [
        'name'              => _x('kategorie', 'taxonomy general name'),
        'singular_name'     => _x('Kategoria', 'taxonomy singular name'),
        'search_items'      => __('Szukaj kategorii'),
        'all_items'         => __('Wszystkie kategorie'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edytuj kategorię'),
        'update_item'       => __('Aktualizuj kategorię'),
        'add_new_item'      => __('Dodaj nowa kategorię'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Kategoria'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'kategoria',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('kategoria', array('kamienie'), $args);
}
add_action('init', 'register_taxonomy_category_kamienie');

// Dodawanie nowego typu postów END




?>