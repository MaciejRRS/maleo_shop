<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
global $product;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" class="single-product-class" <?php wc_product_class( '', $product ); ?>>
    <div class="row">
        <div class="col-lg-6">
            <?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>
        </div>
        <div class="col-lg-6">
            <div class="summary entry-summary">
                <?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>



                <!-- acordeon start -->

                <section class="info-clothers">
                    <div class="accordion-menu">
                        <ul class="acordeon-wrapper-list">
                            <?php if( get_field('dodatkowe_pola_sklad_materialu') ): ?>
                            <li class="content-list-acordeon">
                                <input type="checkbox" checked>
                                <i class="arrow"></i>
                                <h2><?php the_field('sklad_materialu_name','options'); ?></h2>
                                <?php the_field('dodatkowe_pola_sklad_materialu'); ?>
                            </li>
                            <?php endif; ?>

                            <?php if( get_field('dodatkowe_pola_tabela_rozmiarow') ): ?>
                            <li class="content-list-acordeon">
                                <input type="checkbox" checked>
                                <i class="arrow"></i>
                                <h2><?php the_field('tabela_rozmiarow_name','options'); ?></h2>
                                <?php the_field('dodatkowe_pola_tabela_rozmiarow'); ?>
                            </li>
                            <?php endif; ?>

                            <?php if( get_field('dodatkowe_pola_sposob_pielegnacji') ): ?>
                            <li class="content-list-acordeon">
                                <input type="checkbox" checked>
                                <i class="arrow"></i>
                                <h2><?php the_field('sposob_pielegnacji_name','options'); ?></h2>
                                <?php the_field('dodatkowe_pola_sposob_pielegnacji'); ?>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </section>
                <!-- acordeon stop -->









            </div>
        </div>

        <?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	// global $product;
	// $pid = $product->get_id();
	// echo do_shortcode('[product_price]')
	?>


    </div>

    <?php do_action( 'woocommerce_after_single_product' ); ?>