<footer class="footer">
    <?php get_template_part( 'template-parts/footer/footer', 'content' ); ?>
    <?php// get_template_part( 'template-parts/footer/footer', 'copyrights' ); ?>
</footer>

<?php wp_footer(); ?>


<script>
$(window).on('load resize', function() {
    var viewportWidth = $(window).width();
    if (viewportWidth < 1024) {
        $(".berocket_inline_filters").addClass("collapse");
        $(".berocket_ajax_group_filter_title").click(function() {
            $(".berocket_inline_filters").collapse('toggle'); // toggle collapse
        });
    } else {
        $(".berocket_inline_filters").removeClass("collapse");
    }
});
</script>

</body>

</html>