<?php get_header() ;?>




<main id="kamienie">

    <section class="kamienie-section">
        <div class="container">


            <div class="header-wrapper">
                <div class="header-breadcrumps">
                    <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
                </div>


                <div class="title-wrap">
                    <h1 class="page-title"><?php the_title(); ?></h1>
                </div>
            </div>


        </div>





        <!-- start główny repeater -->



        <?php
if( have_rows('gl_repeater_kamienie') ):
    while( have_rows('gl_repeater_kamienie') ) : the_row(); ?>
        <section class="repeater-kamienie-single">
            <div class="container">
                <div class="columns-wrapper-kamienie">
                    <div class="column-left-kamienie">

                        <?php $imageSingleKamienieRepeater = get_sub_field('kamienie_blok_repeater_single'); ?>
                        <img class="image-single-kamienie-repeater"
                            src="<?php echo $imageSingleKamienieRepeater['sizes']['large']; ?>"
                            width="<?php echo $imageSingleKamienieRepeater['sizes']['large']; ?>"
                            height="<?php echo $imageSingleKamienieRepeater['sizes']['large']; ?>"
                            alt="<?php echo esc_attr($imageSingleKamienieRepeater['alt']); ?>" />
                    </div>
                    <div class="column-right-kamienie">
                        <div class="title">
                            <h2><?php the_sub_field('title_kamienie_single_repeater') ?></h2>
                        </div>
                        <div class="text-wyswyg">
                            <?php the_sub_field('text_wysywyg_kamienie_single_repeater') ?>
                        </div>

                        <div class="area-item-icon-txt">
                            <?php
                        if( have_rows('wewn_repeater_kamienie') ):
                            while( have_rows('wewn_repeater_kamienie') ) : the_row(); ?>

                            <div class="wrapper-list-in">
                                <img src="<?php the_sub_field('list_img_kamienie_single_repeater') ?>">
                                <div class="text-title-list">
                                    <h3><?php the_sub_field('text_list_kamienie_single_repeater') ?></h3>
                                </div>
                            </div>


                            <?php  endwhile;
                    else :
                    endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <?php  endwhile;
else :
endif; ?>






        <?php if( get_field('product_tag_kamienie') ): ?>

        <section class="new new__products slider slider_single_kamienie">
            <div class="container">
                <h2 class="section__title section_title_options_kamienie">
                    <?php the_field('kamienie_slider_title','options') ?></h2>

                <div class="slider slider__atelier">


                    <?php
                $idtag = get_field("product_tag_kamienie");
                $args = array(
                    'post_type' => 'product',
                    'orderby' => 'date',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'product_tag',
                            'field' => $idtag,
                    'terms' => $idtag,
                        )
                    )
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) : $i=0;
                while ( $loop->have_posts() ) : $loop->the_post(); $i++; 
                global $product; 
                $pid = $product->get_id();
                $currency = get_woocommerce_currency_symbol();
                $add_to_wishlist = do_shortcode('[yith_wcwl_add_to_wishlist product_id=' . $pid . ']' );
            ?>


                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" class="product__card product">
                        <?php abChangeProductsTitle() ?>
                    </a>

                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>

                </div>



            </div>
        </section>


        <script>
        $(document).ready(function() {
            $('.slider__atelier').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
        </script>

        <?php endif; ?>







        <section class="seo-text-kamienie">
            <div class="text-wrap-seo">
                <div class="container">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>


</main>

<?php get_footer();?>